@extends('main')

@section('content')
    @if (Auth::check() && Auth::user()->isAdmin())

    <div class="row">
    <div class=" col-md-8">
        {!!Form::model($profile, ['route' => ['users.update', $profile->id], 'method' => 'PATCH']) !!}

        {{ Form::label('name', "Name:") }}
        {{ Form::text('name',null, array('class' => 'form-control form-group')) }}
        {{ Form::label('surname', "Surname:") }}
        {{ Form::text('surname',null, array('class' => 'form-control form-group')) }}
        {{ Form::label('country', "Country:") }}
        {{ Form::select('country',array('Lithuania' => 'Lithuania', 'Latvia' => 'Latvia', 'Estonia' => 'Estonia' ), array('class' => 'form-control')) }}
        {{ Form::label('address', "Address:") }}
        {{ Form::text('address',null, array('class' => 'form-control form-group')) }}
        {{ Form::label('city', "City:") }}
        {{ Form::text('city',null , array('class' => 'form-control form-group')) }}
        {{ Form::label('zip_code', "Zip code:") }}
        {{ Form::text('zip_code',null, array('class' => 'form-control form-group')) }}
        {{ Form::label('email', "Email address:") }}
        {{ Form::text('email',null, array('class' => 'form-control form-group')) }}
        {{ Form::label('telephone', "Telephone number:") }}
        {{ Form::text('telephone',null, array('class' => 'form-control form-group')) }}
        {{ Form::label('date', "Date of birth:") }}
        {{ Form::date('date',null, array('class' => 'form-control form-group')) }}
        {{ Form::label('password', "New Password:") }}
        {{ Form::password('password',null, array('class' => 'form-control form-group')) }}
        {{ Form::label('password_confirmation', "Confirm Password:") }}
        {{ Form::password('password_confirmation',null, array('class' => 'form-control form-group')) }}

        {{Form::submit('update your profile', array('class'=> 'btn btn-success btn-lg btn-block', 'style' => 'margin-top:20px'))}}
</div>





        <div class="col-md-4">
            <div class="well">
                <dl class="dl-horizontal">
                    <dt>Created at: </dt>
                    <dd> {{ date('M j, Y, H:i', strtotime($profile->created_at)) }}</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Last updated: </dt>
                    <dd> {{ date('M j, Y, H:i', strtotime($profile->updated_at)) }}</dd>
                </dl>
                <hr>
                <div class="row">
                    <div class="col-sm-6">
                        {!! Html::linkRoute('users.show', 'Cancel', array($profile->id), array('class'=>"btn btn-danger btn-block")) !!}

                    </div>
                    <div class="col-sm-6">
                        {{ Form::submit('Save changes', ['class' => 'btn btn-success btn-block' ]) }}


                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

    @endif




@endsection