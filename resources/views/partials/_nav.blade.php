
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/">Home</a></li>
                <li><a href="{{route('dishes.index')}}">Dishes Menu</a></li>
                @if(Auth::check() && Auth::user()->isAdmin())
                    <li><a href="{{route('reservations.index')}}">Table reservation</a></li>

                @else
                    <li><a href="{{route('reservations.create')}}">Table reservation</a></li>
                @endif
                <li><a href="{{route('contacts')}}">Contacts</a></li>
                @if (Auth::check()) <li><a href="{{route('user.profile')}}"> User profile </a></li> @endif
                @if (Auth::check() && Auth::user()->isAdmin()) <li><a href="{{route('users.index')}}"> Users list</a></li> @endif
                @if (Auth::check() && Auth::user()->isAdmin()) <li><a href="{{route('orders.index')}}"> Orders </a></li> @endif
                @if (Auth::check()) <li><a href="{{route('orders.order')}}"> My orders</a></li> @endif

            </ul>

            @if (Auth::check())
                <ul class="nav navbar-nav navbar-right">
                    <li>
                    <li><a href="{{route('cart')}}">Shopping cart
                            <span class="badge"></span>
                        </a></li>

                            <li><a href="{{ url('/') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>

                    </li>


            @endif

                    @if (!Auth::check())
                    <ul class="nav navbar-nav navbar-right">


                    <li><a href="/login">Login</a></li>
                    <li><a href="/register">Register</a></li>



                </ul>
                    @endif


            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
