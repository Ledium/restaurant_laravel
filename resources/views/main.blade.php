@include('partials.head')

<body>
@include('partials._nav')
<!-- Default Bootstrap Navbar -->



@include('partials._messages')

@yield('content')
@include('partials._footer')

<!-- end of .container -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
@include('partials._js')
@yield('scripts')
</body>

</html>