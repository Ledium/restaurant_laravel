@extends('main')


@section ('content')


    <div class="row">
        <div class="col-md-12">
            <div class=" jumbotron front_header">
                <h1>Welcome to Our Laravel Restaurant!</h1>
                <a href="{{route('dishes.index')}}" class=" my_button btn btn-default  btn-lg">Shop now!</a>

            </div>
        </div>
    </div>



    <!-- end of header .row -->

    </div>
    <div class="row">
    <div class="col-md-12">
    <div class="front">
        <div class="reserve"><i class="fa fa-address-book fa-5x" aria-hidden="true"></i>
        <h3>reserve a table!</h3>
        </div>
        <div class="food"><i class="fa fa-apple fa-5x" aria-hidden="true"></i>
        <h3>choose the dishes you want!</h3>
        </div>
        <div class="contact"><i class="fa fa-compress fa-5x" aria-hidden="true"></i>
        <h3>contact us for more info!</h3>
        </div>
    </div>
    </div>
    </div>


 @endsection

