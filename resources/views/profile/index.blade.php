@extends('main')


@section('content')
    @if (Auth::check() && Auth::user()->isAdmin())
    <div class="row">
        <div class="col-md-10">
            <h1>All users</h1>
        </div>


        <hr>
    </div>

    <div class="class=row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <th>#</th>
                <th>name</th>
                <th>surname:</th>
                <th>Date:</th>
                <th>Country:</th>
                <th>Zip code:</th>
                <th>Address:</th>
                <th>email</th>
                <th>password</th>
                <th>telephone:</th>

                </thead>
                <tbody>
                @foreach($users as $user)

                    <tr>
                        <th>{{$user->id}}</th>
                        <td>{{$user->name}}</td>
                        <td>{{$user->surname}}</td>
                        <td>{{$user->date}}</td>
                        <td>{{$user->country}}</td>
                        <td>{{$user->zip_code}}</td>
                        <td>{{$user->address}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->password}}</td>
                        <td>{{$user->telephone}}</td>
                        <td>{{date( 'M j, Y', strtotime($user->created_at)) }}</td>


                @endforeach

                </tbody>
            </table>
        </div>
    </div>
    @endif



    @endsection