@extends("main")


@section('content')
    @if (Auth::check())
        <div class="row profile_forms">
            <div class=" col-md-offset-2 col-md-8">
    {!!Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'PATCH']) !!}

    {{ Form::label('name', "Name:") }}
    {{ Form::text('name',null, array('class' => 'form-control')) }}
    {{ Form::label('surname', "Surname:") }}
    {{ Form::text('surname',null, array('class' => 'form-control')) }}
    {{ Form::label('country', "Country:") }}
    {{ Form::select('country',array('Lithuania' => 'Lithuania', 'Latvia' => 'Latvia', 'Estonia' => 'Estonia' ), array('class' => 'form-control')) }}
    {{ Form::label('address', "Address:") }}
    {{ Form::text('address',null, array('class' => 'form-control')) }}
    {{ Form::label('city', "City:") }}
    {{ Form::text('city',null , array('class' => 'form-control')) }}
    {{ Form::label('zip_code', "Zip code:") }}
    {{ Form::text('zip_code',null, array('class' => 'form-control')) }}
    {{ Form::label('email', "Email address:") }}
    {{ Form::text('email',null, array('class' => 'form-control')) }}
    {{ Form::label('telephone', "Telephone number:") }}
    {{ Form::text('telephone',null, array('class' => 'form-control')) }}
    {{ Form::label('date', "Date of birth:") }}
    {{ Form::date('date',null, array('class' => 'form-control')) }}
    {{ Form::label('password', "New Password:") }}
    {{ Form::password('password',null, array('class' => 'form-control')) }}
    {{ Form::label('password_confirmation', "Confirm Password:") }}
    {{ Form::password('password_confirmation',null, array('class' => 'form-control')) }}

    {{Form::submit('update your profile', array('class'=> 'btn btn-success btn-lg btn-block', 'style' => 'margin-top:20px'))}}

    {!!Form::close() !!}
        </div>
        </div>

    @endif

@endsection