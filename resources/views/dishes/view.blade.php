@extends('main')

@section ('content')
    @if (Auth::check() && Auth::user()->isAdmin())
    <div class="row">
    <div class="col-md-10">
    <h1>All dishes</h1>
    </div>

    <div class="col-md-2">
    <a href="{{route('dishes.create')}}" class="btn btn-xl btn-block btn-primary btn-h1-spacing">Create New Dish</a>
    </div>
    <hr>
    </div>

    <div class="class=row">
    <div class="col-md-12">
    <table class="table">
    <thead>
    <th>#</th>
    <th>Title</th>
    <th>Description</th>
    <th>Created at:</th>
    <th></th>
    </thead>
    <tbody>
    @foreach($dishes as $dish)

    <tr>
    <th>{{$dish->id}}</th>
    <td>{{$dish->title}}</td>
    <td>{{substr($dish->description, 0, 50)}} {{ strlen($dish->body) > 50 ? "...": " " }}</td>
    <td>{{date( 'M j, Y', strtotime($dish->created_at)) }}</td>
    <td>{{$dish->image}}</td>
    <td><a href="{{route('dishes.show', $dish->id)}}" class="btn btn-default btn-sm">view</a> <a href="{{ route('dishes.edit', $dish->id) }}"class="btn btn-default btn-sm">edit</a></td>
    <td><a href="{{ route('dishes.destroy', $dish->id) }}"class="btn btn-danger btn-sm">delete</a></td>
    </tr>


    @endforeach

    </tbody>
    </table>
    </div>
    </div>

@endif


@endsection