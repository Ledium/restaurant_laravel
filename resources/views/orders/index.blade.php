@extends('main')


@section('content')

    @if (Auth::check() && Auth::user()->isAdmin())
    <div class="row">
        <div class="col-md-10">
            <h1>All orders</h1>
        </div>


        <hr>
    </div>

    <div class="class=row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <th>User ID</th>
                    <th>Total</th>
                    <th>Order_ID</th>
                    <th>Created at:</th>
                <th></th>

                </thead>
                <tbody>
                @foreach($orders as $order)


                    <tr>
                        <td>{{$order->user_id}}</td>
                        <td>{{$order->total}}</td>
                        <td>{{$order->id}}</td>


                        <td>{{date( 'M j, Y', strtotime($order->created_at)) }}</td>
                        <td><a href="{{route('orders.show', $order->id)}}" class="btn btn-default btn-sm">view</a>



                 @endforeach

                </tbody>
            </table>
        </div>
    </div>

    $collection->sum('pages');

    @endif
@endsection