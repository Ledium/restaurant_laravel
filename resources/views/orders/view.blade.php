@extends('main')



@section('content')
    @if (Auth::check())
        <div class="row">
            <div class="col-md-8">>
                <h1>  Your orders</h1>


                {{--<p class="lead">{{$order->user_id}}</p>--}}
                {{--<p class="lead">{{$order->id}}</p>--}}
                {{--<p class="lead">{{$order->total}}</p>--}}
                {{--<p class="lead">{{$order->date}}</p>--}}
            </div>
            <table class="table">
                <thead>
                <th>Prodcut name</th>
                <th>Product ID</th>
                <th>Quantity</th>
                <th>Total price</th>
                <th></th>

                </thead>
                <tbody>
                @foreach($dishes as $dish)

                    <tr>
                        <td>{{$dish->title}}</td>
                        <td>{{$dish->id}}</td>
                        <td>{{$dish->quantity}}</td>
                        <td>{{$dish->total}}</td>

                @endforeach

                </tbody>
            </table>
            {{--<div class="col-md-4">--}}
            {{--<div class="well">--}}
            {{--<dl class="dl-horizontal">--}}
            {{--<dt>Created at: </dt>--}}
            {{--<dd> {{ date('M j, Y, H:i', strtotime($order->created_at)) }}</dd>--}}
            {{--</dl>--}}
            {{--<dl class="dl-horizontal">--}}
            {{--<dt>Last updated: </dt>--}}
            {{--<dd> {{ date('M j, Y, H:i', strtotime($order->updated_at)) }}</dd>--}}
            {{--</dl>--}}
            {{--<hr>--}}
            {{--<div class="row">--}}
            {{--<div class="col-sm-6">--}}
            {{--{!! Html::linkRoute('orders.edit', 'Edit', array($order->id), array('class'=>"btn btn-primary btn-block")) !!}--}}

            {{--</div>--}}
            {{--<div class="col-sm-6">--}}
            {{--{!! Html::linkRoute('orders.destroy', 'Delete', array($order->id), array('class'=>"btn btn-danger btn-block")) !!}--}}

            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}

        </div>
        </div>
    @endif
@endsection