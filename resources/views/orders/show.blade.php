@extends('main')

@section('content')
    @if (Auth::check())
        <div class="row">
            <div class="col-md-8">
                <h1> Your order</h1>

            </div>
            <table class="table">
                <thead>
                <th>Product name</th>
                <th>Product ID</th>
                <th>Price:</th>
                <th>Quantity:</th>
                <th>Total price:</th>
                <th>Price with tax:</th>
                <th>Tax:</th>

                </thead>
                <tbody>
                @foreach($dishes as $dish)
                    <tr>

                        <td>{{$dish->dish->title}}</td>
                        <td>{{$dish->id}}</td>
                        <td>{{$dish->price}} &euro;</td>
                        <td>{{$dish->quantity}}</td>
                        <td>{{$dish->quantity * $dish->price}} &euro;</td>
                        <td>{{$dish->quantity * $dish->price * 1.21}} &euro;</td>
                        <td>{{$dish->quantity * $dish->price * 0.21}} &euro;</td>

                    </tr>
                    @endforeach
                </tbody>
            </table>


        </div>
        </div>
    @endif

    @endsection
