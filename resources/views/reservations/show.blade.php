@extends('main')



@section('content')

    @if (Auth::check() && Auth::user()->isAdmin())
    <div class="row">
    <div class="col-md-8">>
    <h1>  {{ $reservation->title }}</h1>

    <p class="lead">{{$reservation->name}}</p>
        <p class="lead">{{$reservation->phone_number}}</p>
        <p class="lead">{{$reservation->number_of_people}}</p>
        <p class="lead">{{$reservation->time}}</p>
        <p class="lead">{{$reservation->date}}</p>
     </div>
        <div class="col-md-4">
            <div class="well">
               <dl class="dl-horizontal">
                   <dt>Created at: </dt>
                   <dd> {{ date('M j, Y, H:i', strtotime($reservation->created_at)) }}</dd>
               </dl>
                <dl class="dl-horizontal">
                    <dt>Last updated: </dt>
                    <dd> {{ date('M j, Y, H:i', strtotime($reservation->updated_at)) }}</dd>
                </dl>
                <hr>
                <div class="row">
                    <div class="col-sm-6">
                        {!! Html::linkRoute('reservations.edit', 'Edit', array($reservation->id), array('class'=>"btn btn-primary btn-block")) !!}

                    </div>
                    <div class="col-sm-6">
                        {!! Html::linkRoute('reservations.destroy', 'Delete', array($reservation->id), array('class'=>"btn btn-danger btn-block")) !!}

                    </div>
                </div>
            </div>
        </div>

    </div>

    @endif
    @endsection