@extends('main')

@section('content')
    @if (Auth::check() && Auth::user()->isAdmin())
        <div class="row">

            {!! Form::model($reservation, ['route' => ['reservations.update', $reservation->id], 'method' => 'PATCH', 'files' => 1]) !!}
            <div class="col-md-8">
                {{ Form::label('name', "Name:") }}
                {{ Form::text('name',Auth::user()->getFullName(), array('class' => 'form-control')) }}

                {{ Form::label('phone_number', "Phone number:") }}
                {{ Form::text('phone_number',Auth::user()->telephone, array('class' => 'form-control')) }}

                {{ Form::label('date', "Date:") }}
                {{ Form::date('date',null, array('class' => 'form-control')) }}

                {{ Form::label('time', "Time:") }}
                {{ Form::time('time', null, array('class' => 'form-control')) }}

                {{ Form::label('number_of_people', "Number of people:") }}
                {{ Form::number('number of people',null , array('class' => 'form-control')) }}

                {{--{{Form::submit('update reservation', array('class'=> 'btn btn-success btn-lg btn-block', 'style' => 'margin-top:20px'))}}--}}
                {{--{!! Form::close() !!}--}}



            </div>
            <div class="col-md-4">
                <div class="well">
                    <dl class="dl-horizontal">
                        <dt>Created at: </dt>
                        <dd> {{ date('M j, Y, H:i', strtotime($reservation->created_at)) }}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Last updated: </dt>
                        <dd> {{ date('M j, Y, H:i', strtotime($reservation->updated_at)) }}</dd>
                    </dl>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6">
                            {!! Html::linkRoute('reservations.show', 'Cancel', array($reservation->id), array('class'=>"btn btn-danger btn-block")) !!}

                        </div>
                        <div class="col-sm-6">
                            {{ Form::submit('Save changes', ['class' => 'btn btn-success btn-block' ]) }}


                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>


    @endif




@endsection