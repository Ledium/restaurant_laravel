<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class AttachUserRole extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //
        $role_user = Role::where('name', 'User')->first();
        $role_admin = Role::where('name', 'Admin')->first();

        $user = User::where('email', 'kaka@gmail.com')->first();

        $admin = User::where('email', "maka@gmail.com")->first();



        $user->roles()->attach($role_user);
        $user->save();
        $admin->roles()->attach($role_admin);
        $admin->save();
    }
}
