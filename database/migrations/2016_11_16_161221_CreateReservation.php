<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //


        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->required();
            $table->integer('phone_number')->required();
            $table->string('date');
            $table->string('time');
            $table->integer('number_of_people')->required();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('reservations');
    }
}
