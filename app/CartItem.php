<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cartItem extends Model
{
    //
    public function cart()
    {
        return $this->belongsTo('App\Cart');
    }

    public function Dish()
    {
        return $this->belongsTo('App\Dish');
    }
}
