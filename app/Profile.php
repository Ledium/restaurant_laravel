<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'name','surname','country','address','city','zip_code', 'telephone', 'date',  'email', 'password',
    ];
}
