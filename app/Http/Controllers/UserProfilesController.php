<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\profile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Session;


class UserProfilesController extends Controller
{

    protected $redirectTo = '/contact';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = User::all();


        return view('users.index')->withprofiles($profiles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $profile = User::find($id);
        return view('users.show')->withprofile($profile);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        //
        //
        //find the profile and save it as a variable
        $profile = User::find($id);
        // return the view and pass in the  var we previously created
        return view('users.edit')->withprofile($profile);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = User::find($id);
        $this->validate($request, array(
            'name' => 'bail|max:25',
            'surname' => 'bail|max:25',
            'date' => 'date|date_format:Y-m-d|before:2000-1-1',
            'telephone' => ' digits:9|required',Rule::unique('users')->ignore($user->id),
            'email' => '|email|max:255',Rule::unique('users')->ignore($user->id),

        ));

        $inputData = $request->all();

        if ( !empty($inputData['password']))
        {
            $inputData['password'] = bcrypt($inputData['password']);
        } else
        {
            unset($inputData['password']);
        }

        User::find($id)->update($inputData);

        //set flash data with success message

        Session::flash('success', 'profile has been updated successfully');


        return redirect()->route('users.index');


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         * @return \Illuminate\Http\Response
         */

    }
        public
        function destroy($id)
        {
            //
        }

}
