<?php

namespace App\Http\Controllers;
use App\Cart;
use App\CartItem;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{

//    public function __construct()
//    {
//        $this->middleware('auth');
//    }



    public function addItem ($dishId){

        if( Auth::guest())
        {
            return redirect('/login');
        }

        Cart::instance()->addItem($dishId, 1)->save();
        return redirect()->route('cart');

    }
    public function reduceItem ($dishId){

        if( Auth::guest())
        {
            return redirect('/login');
        }

        Cart::instance()->reduceItem($dishId, 1)->save();
        return redirect()->route('cart');

    }

    public function totalQty ()
    {
        Cart::getTotalQuantity()->save();
    }



    public function showCart(){

        return view('cart.view',['cart'=>Cart::instance()]);


    }



    public function removeItem($id){
        Cart::instance()->forget($id)->save();
        return redirect()->route('cart');
    }



}