<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DishOrder extends Model
{

    protected $fillable = [
        'price', 'quantity','dish_id',
    ];

    protected $table = 'dish_order';

    public function dish()
    {
        return $this->belongsTo(Dish::class);
    }









}